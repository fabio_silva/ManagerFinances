var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ui.bootstrap']);

app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

app.config(['$httpProvider',
    function ($httpProvider) {
		$httpProvider.defaults.headers.common["Accept"] = "application/json; charset=utf-8";
        $httpProvider.defaults.headers.common["Content-Type"] = "application/json; charset=utf-8";
        $httpProvider.interceptors.push(commonHttpInterceptor);
        
        function commonHttpInterceptor($q, $rootScope, AppConstants) {
            return {
                'request': function(config) {
                    if (config.url.indexOf(AppConstants.URL_BASE) > -1) {
                        if (!config.headers) config.headers = {};
                        config.headers['Cache-Control'] = 'no-cache';
                        config.headers['Pragma'] = 'no-cache';
                    	
                        $rootScope.loading = true;
                    }
                    return config;
                }, 
                'requestError': function(rejection) {
                    if (rejection.config.url.indexOf(AppConstants.URL_BASE) > -1)
                        $rootScope.loading = false;
                    return $q.reject(rejection);
                }, 
                'response': function(response) {
                    if (response.config.url.indexOf(AppConstants.URL_BASE) > -1)
                        $rootScope.loading = false;
                    return response;
                }, 
                'responseError': function(rejection) {
                    if (rejection.config.url.indexOf(AppConstants.URL_BASE) > -1) {
                        $rootScope.loading = false;

                        if (rejection.status === AppConstants.HTTP_STATUS.FORBIDDEN) {
                        	$rootScope.openModalMessage(AppConstants.MESSAGE.FORBIDDEN);
                        } else if (rejection.status === AppConstants.HTTP_STATUS.UNAUTHORIZED) {
                        	$rootScope.openModalMessage(AppConstants.MESSAGE.UNAUTHORIZED);
                        	$rootScope.logout();
                        } else {
                            if (rejection.status !== AppConstants.HTTP_STATUS.CONFLICT && 
                                    rejection.status !== AppConstants.HTTP_STATUS.NOT_FOUND) {
                            	$rootScope.openModalMessage(AppConstants.MESSAGE.RESPONSE_ERROR, AppConstants.MODAL_MESSAGE.DANGER);
                            }
                        }
                    }
                    return $q.reject(rejection);
                }
            };
        };        
    }
]);

app.config(function ($routeProvider) {
    var createRoute = function(path, controller) {
        return {
            templateUrl: 'view/' + path + '.html',
            controller: controller
        };
    };
     
    var paths = [{path: "login", controller: 'LoginCtrl'}]; 
    paths.forEach(function(item) {
        $routeProvider.when('/' + item.path, createRoute(item.path, item.controller));
    });
    $routeProvider.otherwise({redirectTo: '/login'});    
});

app.constant("AppConstants", {  
    HTTP_STATUS: {
        CONFLICT: 409,
        FORBIDDEN: 403,
        FOUND: 302,
        NOT_FOUND: 404,
        UNAUTHORIZED: 401
    },
    MESSAGE: {
        ACTION_SUCESS: "Action performed with sucess",
        CONSTRAINT_VIOLATION : 'Action not performed, there are records linked to this item that block their exclusion.',
        FORBIDDEN: "Sorry, you do not have permission to this feature.",
        FORM_INVALID: "Please check the highlighted fields and try again, thank you.",
        LOGIN_NOT_FOUND : "Login / Password not found.",
        REQUIRED: "Required field",        
        RESPONSE_ERROR: "An error occurred communicating with the server, please try again later, or contact support.",
        UNAUTHORIZED: "For this feature you need to sign in."
    },
    MODAL_MESSAGE: {
        DANGER: 'danger',
        INFO: 'info',
        QUESTION: 'question'
    },
    URL_BASE: 'rest/'
});

app.run(function(AppConstants, AppService) {
});

app.controller("LoginCtrl", function ($scope, $rootScope, AppConstants, AppService, $http) {
	$scope.MESSAGE = {REQUIRED: AppConstants.MESSAGE.REQUIRED};
	
    $scope.doLogin = function(user) {
    	$rootScope.loggedUser = null;
        
    	$http.post(AppConstants.URL_BASE + "login", user)
        .then(function(response) {
        	$rootScope.loggedUser = response.data;            
        },
        function(response) {
            if (response.status === AppConstants.HTTP_STATUS.NOT_FOUND) {
                $rootScope.openModalMessage(AppConstants.MESSAGE.LOGIN_NOT_FOUND, AppConstants.MODAL_MESSAGE.DANGER);
            }
        });        
    };	
});

/* 
 * input = jQuery object
 * Fonte: https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
 */
function cursorToEnd(input) {
    input.focus();

    // If this function exists...
    if (input.setSelectionRange) {
        // ... then use it (Doesn't work in IE)
        // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
        var len = $(input).val().length * 2;

        input.setSelectionRange(len, len);
    } else {
        // ... otherwise replace the contents with itself
        // (Doesn't work in Google Chrome)
        $(input).val($(input).val());
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Google Chrome)
    input.scrollTop = 999999;        
}