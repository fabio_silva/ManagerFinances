app.service("AppService", function(AppConstants, $location, $uibModal, $route, $rootScope) {
    var _self = this;
    
    _self.loadRoute = function(route, reload) {
        if ($location.path() === route) {
            if (reload) $route.reload();
        } else {
            $location.path(route);
        }
    };
    
    _self.logout = function() {
    	_self.loadRoute("/login");
    };
    
    $rootScope.logout = _self.logout;
    
    _self.openModalMessage = function(message, type, yesFunction, closeFunction) {
        if (!type) type = AppConstants.MODAL_MESSAGE.INFO;
        
        return $uibModal.open({ 
            backdrop: 'static', 
            controller: function($scope, $uibModalInstance, message, type, AppConstants) {
                $scope.MODAL_MESSAGE = {DANGER: AppConstants.MODAL_MESSAGE.DANGER,
                                        INFO: AppConstants.MODAL_MESSAGE.INFO,
                                        QUESTION: AppConstants.MODAL_MESSAGE.QUESTION};
                $scope.message = message;
                $scope.type = type;
                
                if (closeFunction) {
                    $scope.close = closeFunction;
                } else {
                    $scope.close = function() {
                        $uibModalInstance.close();
                    };
                }
                
                if (type === AppConstants.MODAL_MESSAGE.QUESTION) {
                    $scope.no = function() {
                        $uibModalInstance.close();
                    };

                    $scope.yes = yesFunction;
                }
            },
            keyboard: (type !== AppConstants.MODAL_MESSAGE.QUESTION),
            resolve: {
                message: function() { return message; },
                type: function() { return type; }
            },
            templateUrl: 'modalMessage.html' 
        });        
    };  
    
    $rootScope.openModalMessage = _self.openModalMessage;
});