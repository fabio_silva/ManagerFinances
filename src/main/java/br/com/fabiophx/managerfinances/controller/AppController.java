package br.com.fabiophx.managerfinances.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabiophx.managerfinances.model.User;
import br.com.fabiophx.managerfinances.service.UserService;

@RestController
@RequestMapping(value="/rest", 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AppController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<User> login(@RequestBody User user) {
		System.out.println(user.getLogin() + " - " + user.getPassw());
		
		User saved = userService.findByLoginAndPassw(user.getLogin(), user.getPassw());
		if (saved == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<User>(saved, HttpStatus.OK);
	}	
}
