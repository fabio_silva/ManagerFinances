package br.com.fabiophx.managerfinances.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabiophx.managerfinances.model.User;
import br.com.fabiophx.managerfinances.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;	
	
	public User findByLoginAndPassw(String login, String passw) {
		return repository.findByLoginAndPassw(login, passw);
	}
}
