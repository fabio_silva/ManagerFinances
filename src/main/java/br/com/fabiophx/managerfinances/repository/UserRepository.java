package br.com.fabiophx.managerfinances.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.fabiophx.managerfinances.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	public User findByLoginAndPassw(String login, String passw);
}
