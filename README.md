# Project Title
Manager Finances

Project using Spring Boot to control personal finances.

## Built With

Client Side:
* Bootstrap 3.3.7
* jQuery 3.2.1
* AngularJS 1.6.4
* [angular-ui-bootstrap 2.2.0](https://angular-ui.github.io/bootstrap/)

Used webjars (WEB-INF/lib) to use when without internet or when no exists the same in CDN (to cache)

Server Side:
* H2 - Embedded database to no need configure a database to run this app

## Authors

* **Fabio da Silva**
[Blog](http://fabiophx.blogspot.com/) - Portuguese
[LinkedIn](https://br.linkedin.com/in/fabio-da-silva-19651726)

## Next Steps
* Cast to Angular 4
* Use Angular Material instead Bootstrap
* Use JWT